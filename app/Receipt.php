<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    
    /**
     * Get all product entries recorded on the receipt
     */
    function entries() {
    	return $this->hasMany('App\ReceiptEntry');
    }
}
