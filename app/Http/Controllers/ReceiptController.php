<?php

namespace App\Http\Controllers;

use App\Receipt;
use App\Http\Requests\StoreReceipt;
use Illuminate\Http\Request;

use App\Http\Requests;

class ReceiptController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Receipt $Receipt) {
        $receipts = $Receipt->all();

        return $receipts;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Receipt
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Receipt $Receipt, StoreReceipt $request) {
        $Receipt->name    = $request->name;
        $Receipt->paid_by = $request->paid_by;
        $Receipt->save();

        return ['id' => $Receipt->id];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Receipt $Receipt, $id) {
        return $Receipt->with('entries')->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Receipt
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Receipt $Receipt, StoreReceipt $request, $id) {
        $receipt = $Receipt->find($id);

        $receipt->name    = $request->name;
        $receipt->paid_by = $request->paid_by;
        $receipt->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Receipt $Receipt, $id) {
        $Receipt->destroy($id);
    }
}
