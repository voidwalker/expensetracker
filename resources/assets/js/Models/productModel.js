'use strict';

/*jslint es5: true, browser: true, plusplus: true */
/*globals expenses, m */


/**
 * Product model
 */
var Product = function (data) {
    // Defaults
    data = {
        id:         data.id || null,
        receipt_id: data.receipt_id,
        name:       data.name || '',
        price:      data.price || 0,
        quantity:   data.quantity || 1,
    };

    // Properties
    this.id         = m.prop(data.id);
    this.receipt_id = m.prop(data.receipt_id);
    this.name       = m.prop(data.name);
    this.price      = m.prop(data.price);
    this.quantity   = m.prop(data.quantity);
};


/**
 * Total method
 *
 * Calculate the total cost of the product
 *
 * @param {integer} decimals
 * @return {number}
 */
Product.prototype.total = function (decimals) {
    var round    = require('../../../../node_modules/number-round'),
        total    = this.price() * this.quantity();
        decimals = decimals || 2;

    return round(total, decimals);
};


/**
 * isMultiple method
 *
 * Return true if quantity is more than one.
 *
 * @return {boolean}
 */
Product.prototype.isMultiple = function () {
    return this.quantity() > 1;
};


/**
 * roundedPrice method
 *
 * Return a rounded price
 *
 * @param {integer} decimals
 * @return {number}
 */
Product.prototype.roundedPrice = function (decimals) {
    var round    = require('../../../../node_modules/number-round');
        decimals = decimals || 2;

    return round(this.price(), decimals);
};


/**
 * Save product
 */
Product.prototype.save = function () {
    var url = '/receipt/' + this.receipt_id + '/procuct';
    m.request({method: 'POST', url: url, data: this});
};


/**
 * Delete product
 */
Product.prototype.delete = function () {
    var url = '/receipt/' + this.receipt_id + '/procuct';
    m.request({method: 'DELETE', url: url, data: {id: this.id}});
};

module.exports = Product;