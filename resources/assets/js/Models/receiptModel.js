'use strict';

/*jslint es5: true, browser: true, plusplus: true */

/*globals expenses, m */


/**
 * Receipt model constructor
 */
var Receipt = function (data) {
    data = data || {};
    
    // Set default values
    data = {
        id:      data.id        || null,
        name:    data.name      || '',
        paid_by: data.paid_by   || '',
    };

    // Assign values
    this.id         = m.prop(data.id);
    this.name       = m.prop(data.name);
    this.paid_by    = m.prop(data.paid_by);
    this.products   = Array();

    // Load receipt if id is set
    if(this.id()) {
        this.load();
    }
};


/**
 * Total method
 *
 * Calculates the total of all products
 *
 * @param {integer} decimals
 * @return {number}
 */
Receipt.prototype.total = function (decimals) {
    // Default precision
    decimals = decimals || 2;

    // Calculate total of all products in receipt
    var i, product, value = 0;
    for (i = 0; i < this.products.length; i++) {
        product = this.products[i];
        value = value + (Number(product.price(), 10) * Number(product.quantity(), 10));
    }

    // Round value
    var round = require('../../../../node_modules/number-round');
    return round(value, decimals);
};


/**
 * Save method
 *
 * Save model data to backend API
 *
 * @return {boolean}
 */
Receipt.prototype.save = function () {
    return m.request({method: 'POST', url: '/api/receipt', data: this });
};


/**
 * Load method
 *
 * Get model data from backend API
 *
 * @return {object}
 */
Receipt.prototype.load = function () {
    var url = '/api/receipt/' + this.id(),
        self = this;

    m.request({method: 'GET', url: url}).then(function (res) {
        self.name(res.name);
        self.paid_by(res.paid_by);
    });

};

module.exports = Receipt;