/*jslint browser: true */
/*globals m, ReceiptComponent */


/**
 * Register components
 */
var ReceiptComponent = require('./Components/Receipt/receiptComponent');
var createReceiptComponent = require('./Components/Receipt/createReceiptComponent');


/**
 * Define routes
 */
 m.route.mode = 'hash';
 m.route(document.body, '/', {
 	'/': {
 		controller: createReceiptComponent.controller, 
 		view: createReceiptComponent.view 
 	},
 	'/receipt/:id': {
 		controller: ReceiptComponent.controller,
 		view: ReceiptComponent.view 
 	}
 });

// Mount app
//m.mount(document.body, {controller: createReceiptComponent.controller, view: createReceiptComponent.view});

console.log('Running');
