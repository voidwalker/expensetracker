/*globals Receipt, Product */

var kvitto = new Receipt();
kvitto.push(new Product({ name: 'Sill', price: 25 }));
kvitto.push(new Product({ name: 'Mjölk', price: 12.5 }));

console.log(kvitto.total());
