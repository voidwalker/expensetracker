'use strict';

/*jslint browser: true, plusplus: true */
/*globals m, Receipt, Product */


/**
 * Models
 */
var Receipt = require('../../Models/receiptModel');


/**
 * Component
 */
var Component = {};


/**
 * View model
 */

Component.vm = (function () {
	var vm = {};

    vm.init = function () {
        // Load receipt model
        vm.receipt = new Receipt();

        // Placeholder fields
        vm.name = m.prop('');
        vm.paid_by = m.prop('');

        // Create new receipt and switch to it
        vm.add = function () {
            if (vm.name() && vm.paid_by()) {

            	// Prep model
            	vm.receipt.name(vm.name());
            	vm.receipt.paid_by(vm.paid_by());

            	// Save data and redirect to receipt
            	this.receipt.save().then(function (response) {
            		console.log('Got this response: ', response);
            		if(response.id) {
            			m.route('/receipt/' + response.id);
            		}
            	}, this.error);
            }
        };

        // Remove product from receipt
        vm.remove = function (index) {
            vm.list.splice(index, 1);
        };
    };

    return vm;
}());


/**
 * Controller
 */
Component.controller = function () {
    Component.vm.init();
};

/**
 * View
 */
Component.view = function () {
	return m('div.receipt-component.receipt-create', [
		m('h2', 'Create receipt'),
		m('form', [
			m('div.form-group', [
				m('label', {for: 'receipt-paid-by'}, 'Receipt name:'),
				m('input.form-control#receipt-name', {
					onchange: m.withAttr('value', Component.vm.name),
					value: Component.vm.name()
				})
			]),
			m('div.form-group', [
				m('label', {for: 'receipt-paid-by'}, 'Paid by:'),
				m('input.form-control#receipt-paid-by', {
					onchange: m.withAttr('value', Component.vm.paid_by),
					value: Component.vm.paid_by()
				})
			]),
			m('div.form-group', [
				m('button.btn.btn-primary', {
					onclick: Component.vm.add.bind(Component.vm)
				}, 'Create receipt')
			])
		])
	]);
}

module.exports = Component;