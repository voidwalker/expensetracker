'use strict';
/*jslint browser: true, plusplus: true */
/*globals m, Receipt, Product */


/**
 * Models
 */
var Receipt = require('../../Models/receiptModel');
var Product = require('../../Models/productModel');


/**
 * Receipt Component
 */

var ReceiptComponent = {};


// View Model
ReceiptComponent.vm = (function () {

    var vm = {};
    vm.init = function () {
        // Load receipt model
        vm.receipt = new Receipt({id: m.route.param('id')});

        // Product list
        vm.list = vm.receipt.products;

        // Placeholder fields (with defaults)
        vm.name     = m.prop('');
        vm.price    = m.prop(0);
        vm.quantity = m.prop(1);

        // Add new product to receipt
        vm.add = function () {
            if (vm.name() && vm.price() >= 0 && vm.quantity() > 0) {

                // Instantiate model
                var product = new Product({
                    receipt_id: vm.receipt.id(),
                    name: vm.name(),
                    price: vm.price(),
                    quantity: vm.quantity()
                });

                // Add to product list
                vm.list.push(product);

                // Send data to backend
                product.save();

                // Reset View Model
                vm.name('');
                vm.price(0);
                vm.quantity(1);
            }
        };

        // Remove product from receipt
        vm.remove = function (index) {
            vm.list.splice(index, 1);
        };
    };

    return vm;
}());


// Controller
ReceiptComponent.controller = function () {
    ReceiptComponent.vm.init();
};


// Views
ReceiptComponent.view = function () {
    return [
        m('div.receipt-component', [
            m('h2', ReceiptComponent.vm.receipt.name()),
            m('form', [
                m('div.form-group', [
                    m('label', {for: 'name'}, 'Product name'),
                    m('input.form-control#name', {
                        onchange: m.withAttr('value', ReceiptComponent.vm.name),
                        value: ReceiptComponent.vm.name()
                    }),
                ]),
                m('div.row', [
                    m('div.col-md-6', [
                        m('div.form-group', [
                            m('label', {for: 'quantity'}, 'Quantity'),
                            m('input.form-control#quantity', {
                                onchange: m.withAttr('value', ReceiptComponent.vm.quantity), 
                                value: ReceiptComponent.vm.quantity()
                            }),
                        ]),
                    ]),
                    m('div.col-md-6', [
                        m('div.form-group', [
                            m('label', {for: 'price'}, 'Price'),
                            m('input.form-control#price', {
                                onchange: m.withAttr('value', ReceiptComponent.vm.price),
                                value: ReceiptComponent.vm.price()
                            }),
                        ]),
                    ]),
                ]),
            ]),

            m('button.btn.btn-primary.btn-block', {
                onclick: ReceiptComponent.vm.add
            }, 'Add product'),
            m('hr'),
            m('table.table.table-striped', [
                m('tbody', [
                    ReceiptComponent.vm.list.length === 0 ? m('tr.no-products', [ m('td', { colspan: 4 }, 'Please add products')]) : null,
                    ReceiptComponent.vm.list.map(function (product, index) {
                        return m('tr', [
                            m('td', [
                                m('button.btn.btn-danger.pull-left', {
                                    onclick: ReceiptComponent.vm.remove.bind(ReceiptComponent.vm, index)
                                }, [
                                    m('i.glyphicon.glyphicon-trash')
                                ])
                            ]),
                            m('td', [
                                product.name(),
                            ]),
                            m('td', [
                                product.isMultiple() ? product.quantity() + ' x ' : '',
                                product.roundedPrice(), ' kr.',
                            ]),
                            m('td', [
                                m('span.pull-right', [
                                    product.total(), ' kr'
                                ]) 
                            ]),
                        ]);
                    }),
                ]),
            ]),
            m('hr'),
            m('div.pull-right', [ 'Totalt: ', ReceiptComponent.vm.receipt.total(), ' kr.' ])
        ])
    ];
};

module.exports = ReceiptComponent;