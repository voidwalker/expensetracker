# Expense Tracker

A simple application to keep track of my economy.

It uses PHP 7 and the Laravel MVC framework as backend, and the extremely lightweight Mithril framework to create the SPA.

## Installation

Easiest way to try it out is to use Laravel Homestead.

## Usage

Register your receipts by giving them a name, or some other identification.

Add all products (name, quantity and price).

The dashboard will now give you that overview you always wished you had.

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## License

MIT